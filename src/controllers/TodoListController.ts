import Logger from '../loaders/logger';
import IOutputBaseResponse from '../interfaces/IOutputBaseResponse';
import BaseController from './BaseController';
// import WikiService from '../services/WikiService';
import mongo from '../loaders/mongodb';

const ObjectId = require('mongodb').ObjectId;

export default class TodoListController extends BaseController {
    /**
     * Get Header
     * @return {Promise<IOutputBaseResponse|null>}
     */
    async list(): Promise<IOutputBaseResponse | null> {
        // Output response fields
        let output: IOutputBaseResponse = this.getOutputBaseResponse();

        try {
            const response = await mongo.db('25giin_test').collection('todos_list').find().toArray()
            output.data = response;
            output.message = 'Todo List';


            return output;
        } catch (error) {
            Logger.error('TodoListController@list error');
            Logger.error(error);

            output.error = true;
            output.message = 'Error getting todo list';
            output.errors.push(output.message);

            return output;
        }
    }

    /**
     * Get Header
     * @return {Promise<IOutputBaseResponse|null>}
     */
    async store(name: string): Promise<IOutputBaseResponse | null> {
        // Output response fields
        let output: IOutputBaseResponse = this.getOutputBaseResponse();

        console.log("name");
        console.log(name);

        try {
            const response = await mongo.db('25giin_test').collection('todos_list')
                .insertOne({
                    name: name
                });
            output.data = response;
            output.message = 'Todo List Store';


            return output;
        } catch (error) {
            Logger.error('TodoListController@store error');
            Logger.error(error);

            output.error = true;
            output.message = 'Error storing todo list';
            output.errors.push(output.message);

            return output;
        }
    }

    /**
     * Get Header
     * @return {Promise<IOutputBaseResponse|null>}
     */
    async update(id: string, name: string): Promise<IOutputBaseResponse | null> {
        // Output response fields
        let output: IOutputBaseResponse = this.getOutputBaseResponse();

        try {
            const response = await mongo.db('25giin_test').collection('todos_list')
                .findOneAndUpdate({
                        _id: new ObjectId(id)
                    },
                    {
                        $set: {
                            name: name,
                        }
                    }
                );

            output.data = response;
            output.message = 'Todo List Update';

            return output;
        } catch (error) {
            Logger.error('TodoListController@update error');
            Logger.error(error);

            output.error = true;
            output.message = 'Error updating todo list';
            output.errors.push(output.message);

            return output;
        }
    }

    /**
     * Get Header
     * @return {Promise<IOutputBaseResponse|null>}
     */
    async delete(id: string): Promise<IOutputBaseResponse | null> {
        // Output response fields
        let output: IOutputBaseResponse = this.getOutputBaseResponse();

        try {
            const response = await mongo.db('25giin_test').collection('todos_list')
                .deleteOne({
                    _id: new ObjectId(id)
                });

            output.data = response;
            output.message = 'Todo List Remove';


            return output;
        } catch (error) {
            Logger.error('TodoListController@delete error');
            Logger.error(error);

            output.error = true;
            output.message = 'Error removing todo list';
            output.errors.push(output.message);

            return output;
        }
    }
}
