import IOutputBaseResponse from '../interfaces/IOutputBaseResponse';

export default class BaseController {
  /**
   * Get Output Base Response
   * @return {IOutputBaseResponse} Output Base Response
   */
  getOutputBaseResponse(): IOutputBaseResponse {
    return {
      error: false,
      errors: [],
      message: null,
      data: null,
    };
  }
}
