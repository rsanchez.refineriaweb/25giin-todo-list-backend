import winston from 'winston';
import winstonRotator from 'winston-daily-rotate-file';
import config from '../config';

const transports = [
  //
  // - Write all logs with level `error` and below to `error.log`
  // - Write all logs with level `info` and below to `combined.log`
  //
  new winstonRotator({
    dirname: './logs',
    filename: 'error-%DATE%.log',
    level: 'error',
    datePattern: 'YYYY-MM-DD',
  }),
  new winstonRotator({
    dirname: './logs',
    level: 'info',
    filename: 'combined-%DATE%.log',
    datePattern: 'YYYY-MM-DD',
  }),
];

const LoggerInstance = winston.createLogger({
  level: config.logs.level,
  levels: winston.config.npm.levels,
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss',
    }),
    winston.format.errors({ stack: true }),
    winston.format.splat(),
    winston.format.json(),
  ),
  transports,
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
  LoggerInstance.add(
    new winston.transports.Console({
      format: winston.format.simple(),
    }),
  );
  // LoggerInstance.add(new winstonRotator({
  //   dirname: './logs',
  //   filename: 'debug-%DATE%.log',
  //   level: 'debug',
  //   datePattern: 'YYYY-MM-DD'
  // }));
}

export default LoggerInstance;
