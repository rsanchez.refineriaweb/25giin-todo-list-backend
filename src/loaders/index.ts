import expressLoader from './express';
import mongodbLoader from './mongodb';

export default async ({ expressApp }) => {

    await mongodbLoader;

    await expressLoader({ app: expressApp });
};
