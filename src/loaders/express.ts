import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import routes from '../api';
import config from '../config';

const path = require('path');

export default ({ app }: { app: express.Application }) => {
    /**
     * Health Check endpoints
     */
    app.get('/status', (req, res) => {
        res.status(200).end();
    });
    app.head('/status', (req, res) => {
        res.status(200).end();
    });

    app.get('/', (req, res) => {
        return res
            .status(200)
            .send({
                error: false,
                error_code: 0,
                message: 'Still a live',
                data: null,
            })
            .end();
    });

    // Useful if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc)
    // It shows the real origin IP in the heroku or Cloudwatch logs
    app.enable('trust proxy');

    // The magic package that prevents frontend developers going nuts
    // Alternate description:
    // Enable Cross Origin Resource Sharing to all origins by default
    // app.use(cors());
    app.use(cors({ credentials: true, origin: true }));

    // Some sauce that always add since 2014
    // "Lets you use HTTP verbs such as PUT or DELETE in places where the client doesn't support it."
    // Maybe not needed anymore ?
    app.use(require('method-override')());

    // Middleware that transforms the raw string of req.body into json
    app.use(bodyParser.json());

    app.use(
        bodyParser.urlencoded({
            extended: true,
        }),
    );


    // Load API routes
    app.use(config.api.prefix, routes());

    // Public folder for static files
    app.use(express.static('public'));

    /// error handlers
    app.use((err, req, res, next) => {
        /**
         * Handle method OPTIONS
         */
        if (req.method === 'OPTIONS') {
            console.log('!OPTIONS');
            let headers = {};
            // IE8 does not allow domains to be specified, just the *
            // headers["Access-Control-Allow-Origin"] = req.headers.origin;
            headers['Access-Control-Allow-Origin'] = '*';
            headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE, OPTIONS';
            headers['Access-Control-Allow-Credentials'] = false;
            headers['Access-Control-Max-Age'] = '86400'; // 24 hours
            headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept';
            res.writeHead(200, headers);
            res.end();
        }
        return next(err);
    });

    /// catch 404 and forward to error handler
    app.use((req, res, next) => {
        const err = new Error('Not Found');
        err['status'] = 404;
        next(err);
    });

    /// error handlers
    app.use((err, req, res, next) => {
        /**
         * Handle 401 thrown by express-jwt library
         */
        if (err.name === 'UnauthorizedError') {
            return res
                .status(err.status)
                .send({ message: err.message })
                .end();
        }
        return next(err);
    });

    app.use((err, req, res, next) => {
        res.status(err.status || 500);
        res.json({
            errors: {
                message: err.message,
            },
        });
    });

    // Set Views folder
    app.set('views', path.join(__dirname, '/../../', 'views'));

    // Set EJS Templates Engine
    app.set('view engine', 'ejs');
};
