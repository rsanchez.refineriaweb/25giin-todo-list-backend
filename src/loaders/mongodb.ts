import {MongoClient} from 'mongodb';
import Logger from "./logger";

const client = new MongoClient(process.env.MONGODB_URI || "mongodb://localhost", {useUnifiedTopology: true});

client.connect().then(client => {
    Logger.info('Connected to Database')
}).catch(error => Logger.error(error));

export default client;
