// const express = require('express');
// const app = express();
// const port = process.env.PORT || 3000;
//
// app.get('/', (req, res) => {
//     res.send('Hello, world!');
// })
//
// app.listen(port, () => {
//     console.log(`Example app listening at http://localhost:${port}`);
// });


import config from './config';
import express from 'express';
import Logger from './loaders/logger';

async function startServer() {
    const app = express();

    /**
     * A little hack here
     * Import/Export can only be used in 'top-level code'
     * Well, at least in node 10 without babel and at the time of writing
     * So we are using good old require.
     **/
    await require('./loaders')
        .default({ expressApp: app })
        .catch(err => Logger.error(err));

    app.listen(config.port, err => {
        if (err) {
            Logger.error(err);
            process.exit(1);
            return;
        }
        Logger.info(`
      ################################################
         Server listening on port: ${config.port}
      ################################################
    `);
    });

}

startServer();
