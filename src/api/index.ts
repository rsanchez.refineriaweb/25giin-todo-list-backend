import { Router } from 'express';
import mongo from './routes/mongo';

// guaranteed to get dependencies
export default () => {
    const app = Router();

    mongo(app);

    return app;
};
