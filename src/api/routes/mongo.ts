import { Router } from 'express';
import TodoListController from "../../controllers/TodoListController";

// eslint-disable-next-line new-cap
const route = Router();

export default (app: Router) => {
    // Route Prefix
    app.use('/todo', route);

    // Get all
    route.get('/', async (req, res) => {
        const todoListController = new TodoListController();
        const response = await todoListController.list();

        return res.send(response);
    });

    route.post('/store', async (req, res) => {
        const todoListController = new TodoListController();
        const name: string = req.body.name;
        const response = await todoListController.store(name);

        return res.send(response);
    });

    route.put('/update', async (req, res) => {
        const todoListController = new TodoListController();
        const id: string = req.body._id;
        const name: string = req.body.name;

        const response = await todoListController.update(id, name);

        return res.send(response);
    });

    route.delete('/:_id/delete', async (req, res) => {
        const todoListController = new TodoListController();
        const id: string = req.params._id;

        const response = await todoListController.delete(id);

        return res.send(response);
    });
};
