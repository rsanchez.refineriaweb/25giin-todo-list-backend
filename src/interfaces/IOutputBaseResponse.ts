export default interface IOutputBaseResponse {
  error: boolean;
  errors: Array<string>;
  message: string | null;
  data: any;
}
